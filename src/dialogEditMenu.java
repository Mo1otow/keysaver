import javax.swing.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class dialogEditMenu extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField siteField;
    private JTextField loginField;
    private JTextField passwordField;

    public dialogEditMenu(String title, String login, String password) {
        setSize(500, 300);
        setVisible(true);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);


        String siteName = title;

        siteField.setText(title);
        loginField.setText(login);
        passwordField.setText(password);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }


    private void onOK() {
        String titleValue = siteField.getText();
        String loginValue = loginField.getText();
        String passwdValue = passwordField.getText();
        String sqlAcc = "UPDATE Accounts SET login='" + loginValue + "', password='"+ passwdValue + "' WHERE title='" + titleValue + "';";
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/POBD?autoReconnect=true&useSSL=false", "root", "1")){

            Statement statement = conn.createStatement();

            statement.executeUpdate(sqlAcc);
            System.out.printf("Edit success");
            System.out.println(siteField + "__" + loginValue + "__" + passwdValue );
        } catch (Exception ex){
            System.out.println(ex);
            System.out.println("Шото пошло не так");
        }
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        siteField = null;
        loginField = null;
        passwordField = null;

        dispose();
    }

    public static void main(String[] args) {
        dialogEditMenu dialog = new dialogEditMenu("", "", "");
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
