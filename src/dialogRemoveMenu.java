import javax.swing.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Properties;

public class
dialogRemoveMenu extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;

    FileInputStream fis;
    Properties property = new Properties();

    public dialogRemoveMenu(String title) {
        setSize(500, 300);
        setVisible(true);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK(title);
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK(String title) {

        String sqlAcc = "DELETE FROM Link where title= '" + title + "';";


        try {

            fis = new FileInputStream("src/database.properties");
            property.load(fis);

            String urlSQL = property.getProperty("url");
            String loginSQL = property.getProperty("login");
            String passwordSQL = property.getProperty("password");

            try (Connection conn = DriverManager.getConnection(urlSQL, loginSQL, passwordSQL)) {

            Statement statement = conn.createStatement();

            statement.executeUpdate(sqlAcc);
            System.out.printf("Remove success");
        } catch (Exception ex){
            System.out.println(ex);
            System.out.println("Шото пошло не так");
        }
        } catch (Exception except) {

            System.err.println("ОШИБКА: Файл свойств отсуствует!");

        }
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        dialogRemoveMenu dialog = new dialogRemoveMenu("");
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
