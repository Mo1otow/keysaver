import org.apache.commons.dbutils.DbUtils;
import javax.swing.*;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Properties;
import java.util.Vector;

public class guiForm extends JFrame {
    private JButton addValue;
    private JButton editPost;
    private JTextField searchField;
    private JButton removePost;
    private JPanel rootPanel;
    private JTable Account;
    private JButton refreshButton;
    private JButton deleteAccountButton;

    FileInputStream fis;
    Properties property = new Properties();

    public guiForm(int uID) {
        if (uID < 1 ){
            dispose();
        }
        System.out.println( "\n" + uID);
        setContentPane(rootPanel);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(500, 500);




        String[] columnNames = {
                "Site",
                "Login",
                "Password"};
        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(columnNames);
        Account.setModel(model);

        deleteAccountButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new dialogDeleteAcc(uID);
            }
        });

        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                model.setRowCount(0);
                String sql = "SELECT title FROM Link WHERE uID='" + uID + "';";
                try {

                    fis = new FileInputStream("src/database.properties");
                    property.load(fis);

                    String urlSQL = property.getProperty("url");
                    String loginSQL = property.getProperty("login");
                    String passwordSQL = property.getProperty("password");

                    try (Connection conn = DriverManager.getConnection(urlSQL, loginSQL, passwordSQL)) {
                        ;

                        Statement statement = conn.createStatement();
                        Statement state = conn.createStatement();

                        ResultSet rs = statement.executeQuery(sql);

                        while (rs.next()) {
                            String titles = rs.getString(1);

                            String sqlView = "SELECT * FROM Accounts WHERE title='" + titles + "';";
                            ResultSet resultSet = state.executeQuery(sqlView);
                            resultSet.next();
                            String title = resultSet.getString(1);
                            String login = resultSet.getString(2);
                            String password = resultSet.getString(3);

                            model.addRow(new Object[]{title, login, password});

                        }


                    } catch (Exception ex) {
                        System.out.println(ex);
                        System.out.println("Это не работает");
                    }
                } catch (Exception except) {

                    System.err.println("ОШИБКА: Файл свойств отсуствует!");

                }
            }
        });
  TableRowSorter<TableModel> rowSorter
                = new TableRowSorter<>(Account.getModel());
            Account.setRowSorter(rowSorter);
        String text = searchField.getText();
        if (text.trim().length() == 0) {
            rowSorter.setRowFilter(null);
        } else {
            rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
        }

        searchField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {

                String text = searchField.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }

            }

            @Override
            public void removeUpdate(DocumentEvent e) {

                String text = searchField.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }

            }

            @Override
            public void changedUpdate(DocumentEvent e) {

                throw new UnsupportedOperationException("Not supported yet.");

            }
        });


        searchField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


            }
        });

        addValue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new dialogAddMenu(uID);
            }
        });
        Account.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int countClick = e.getClickCount();
                int row = Account.rowAtPoint(e.getPoint());
                String title = (String) Account.getValueAt(row, 0);
                String login = (String) Account.getValueAt(row, 1);
                String password = (String) Account.getValueAt(row, 2);

                removePost.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        new dialogRemoveMenu(title);
                    }
                });

                editPost.addActionListener(new ActionListener() {
                    @Override
                        public void actionPerformed(ActionEvent e) {
                //двойной шелчок
                         //путь попроще без selectionMode
                        if (row > -1) {
                            new dialogEditMenu(title, login, password);
                        }
                    }
                });

            }
        });

    }
}